#!/bin/busybox sh
input_args=${@}
arraylength=${#input_args[@]}
AL=2
AR=10
AU=13
AT=14
AZ=15
BE=21
BA=27
BR=30
BG=33
CA=38
CL=43
CR=52
HR=54
CY=56
CZ=57
DK=58
EG=64
EE=68
FI=73
FR=74
GE=80
DE=81
GR=84
HK=97
HU=98
IS=99
IN=100
ID=101
IR=104
IL=105
IT=106
JP=108
LV=119
LU=126
MK=128
MY=131
MX=140
MD=142
NL=153
NZ=156
NO=163
PL=174
PT=175
RO=179
RU=180
RS=192
SG=195
SK=196
SI=197
ZA=200
KP=114
ES=202
SE=208
CH=209
TW=211
TH=214
TR=220
UA=225
AE=226
UK=227
US=228
VN=234
eval country_code="\$$input_args"
eval country_name="\$$country_code"
#echo $input_args $country_code 
TWOCHARS=`echo $input_args | grep -xE '^\w{2}' `
MANYCHARS=`echo $input_args | grep -xE '^\w{2}\w+' `

if [ $TWOCHARS ]
then
	if [ $country_code ]
	then
		echo "\"$input_args($country_code)\" is a valid country code"
	fi
elif [ $MANYCHARS ]
then
	echo "$input_args has many chars"
	RECOMMENDED_SERVER="$input_args.nordvpn.com"
	echo $RECOMMENDED_SERVER
fi

