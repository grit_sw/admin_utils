from setuptools import setup

setup(
        name='changevpn',
        version='0.1',
        py_modules=['changevpn'],
        description='Change vpn server of grit easily',
        author="Ilozulu Chris",
        author_email='chidiuso@grit.systems',
        url="",
        license="MIT License",
        platform="",
        install_requires=[
            'Click', 'selenium', 'spur'
        ],
        entry_points='''
        [console_scripts]
        changevpn=changevpn:cli
        ''',
)
