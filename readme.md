This repo contains code that automates the change of VPN.

# Setup
- Download [geckodriver](https://github.com/mozilla/geckodriver/releases/tag/v0.24.0) for firefox or [chromedriver](https://sites.google.com/a/chromium.org/chromedriver/downloads) for firefox and add  to your path.
- Put the necessary crt and key files for the vpn servers into the folder:`~\GRIT_VPNS`
- Run python3 setup.py to install the cli app.

## Usage
### Changing vpn
- Use `changevpn change --server SERVERNAME` where SERVERNAME is the name of the vpn server you want to change to.
## Rebooting router
- Use `changevpn reboot` to reboot router