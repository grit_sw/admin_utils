import glob
import json
import os
import time

import click
import spur
from selenium import webdriver
from selenium.common.exceptions import WebDriverException

GRIT_VPNS = "GRIT_VPNS"

SSH_USER = "root"
USERNAME = "admin"
PASSWORD = "CePrNd2K18"


class Config(object):
    pass


pass_config = click.make_pass_decorator(Config, ensure=True)


@click.group()
@click.option("--username", type=click.STRING, required=False, help="Username to login in with", default=USERNAME)
@click.option("--password", type=click.STRING, required=False,  default=PASSWORD,
              help="Password for username")
@pass_config
def cli(config, username, password):
    config.username = username
    config.password = password


@cli.command("change", short_help="Command to help change vpn")
@click.option('--server', prompt="Name of vpn server eg:es20:")
@pass_config
def change_vpn(config, server):
    BrowserShit(username=config.username, password=config.password, server=server).change_vpn()


@cli.command("reboot", short_help="Reboot router")
@pass_config
def router_reboot(config):
    BrowserShit(username=config.username, password=config.password, server=None).reboot_router()


class BrowserShit:
    def __init__(self, username, password, server, router_ip=None):
        self.driver = ""
        self.username = username
        self.password = password
        self.server = server
        self.element = ""
        self.key_file = ""
        self.cert_file = ""
        self.route_ip = router_ip or "192.168.1.254"
        self.shell = ""

    def open_browser(self):
        try:
            self.driver = webdriver.Firefox()
        except WebDriverException:
            try:
                self.driver = webdriver.Chrome()
            except WebDriverException:
                click.echo(click.style("You need to install the chromedriver or geckodriver "
                                       "for chrome or firefox respectively.\n geckodriver can be gotten from  https://github.com/mozilla/geckodriver/releases/tag/v0.24.0 and chromedriver can be gotten from https://sites.google.com/a/chromium.org/chromedriver/downloads ", fg="red"))
        self.driver.implicitly_wait(30)

    def open_vpn_settings_page(self):
        self.driver.get(f"http://{self.username}:{self.password}@{self.route_ip}/PPTP.asp")

    def change_server_name(self):
        # todo throw exception when sth goes wrong
        self._prepare_element_for_change('openvpncl_remoteip')
        self.element.send_keys(f"{self.server}.nordvpn.com")

    def _prepare_element_for_change(self, element_name):
        self.element = self.driver.find_element_by_name(element_name)
        self.element.clear()

    def change_server_credentails(self):
        self._change_server_keys()
        self._change_server_certificate()

    def _change_server_keys(self):
        new_key = self.read_file(self.key_file)
        self._prepare_element_for_change("openvpncl_tlsauth")
        self.element.send_keys(new_key)

    def _change_server_certificate(self):
        new_cert = self.read_file(self.cert_file)
        self._prepare_element_for_change("openvpncl_ca")
        self.element.send_keys(new_cert)

    def close_browser(self):
        self.driver.quit()

    @staticmethod
    def read_file(path):
        with open(path) as fh:
            file_content = fh.read()
        return file_content

    def find_credentials_files(self):
        # path to credentials folder.
        credentials_dir = f"{os.environ['HOME']}/{GRIT_VPNS}"  # todo abstract to json
        for file_name in glob.iglob("{dir}/{server}*".format(dir=credentials_dir, server=self.server)):  # generator, search immediate subdirectories
            if file_name.endswith(".key"):
                self.key_file = file_name
            if file_name.endswith(".crt"):
                self.cert_file = file_name
        if self.key_file == "" or self.cert_file == "":
            raise FileNotFoundError(f"Key or certificate file for {self.server} not found!")

    def apply_settings(self):
        self.element = self.driver.find_element_by_name("apply_button")
        self.element.click()

    def save_settings(self):
        self.element = self.driver.find_element_by_name("save_button")
        self.element.click()

    def enter_route(self):
        self.shell = spur.SshShell(
                        hostname=self.route_ip,
                        username="root",
                        password=self.password
                    )
        result = self.shell.run(["curl", "-k" , "https://nordvpn.com/wp-admin/admin-ajax.php?action=servers_recommendations"])
        self.preffered_server = json.loads(result.output)[0]['hostname']
        print(self.preffered_server)

    def enable_open_vpn_gui(self):
        self.shell = spur.SshShell(
            hostname=self.route_ip,
            username=SSH_USER,
            password=self.password
        )
        click.echo(click.style(text="Enabling gui....", blink=True, fg='blue'))
        self.shell.run(
            ["nvram", "set", "openvpncl_enable=1"])
        self.shell.run([
            "nvram", "commit"
        ])
        time.sleep(5)

    def reboot_router(self):
        self.shell = spur.SshShell(
            hostname=self.route_ip,
            username=SSH_USER,
            password=self.password
        )
        click.echo(click.style(text="Rebooting router....", blink=True, fg='blue'))
        self.shell.run(
            ["reboot"])
        click.echo(click.style(text="Router rebooted", fg='green'))

    def change_vpn(self):
        self.find_credentials_files()
        self.enable_open_vpn_gui()
        self.open_browser()
        time.sleep(2)
        self.open_vpn_settings_page()
        self.change_server_name()
        self.change_server_credentails()
        self.save_settings()
        time.sleep(3)
        self.apply_settings()
        self.close_browser()
        click.echo(click.style(text="VPN CHANGED", fg="green", bg="blue"))


if __name__ == '__main__':
    c = BrowserShit(username="admin", password="CePrNd2K18", server="es20")
    c.change_vpn()

