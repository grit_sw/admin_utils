#!/bin/sh
#
# latency.sh measures the latency between two network interfaces and report the difference 
# this script is meant to run as a periodic cronjob on dd-wrt router to determine the added latency of a VPN server
# the output of the script may be used to trigger a new VPN server selection
# 15.02.2019 Ifedayo Oladapo
#
#-----------------------------------------------------------------------------------------------------------------
NORDVPN_USERNAME="yoda@grit.systems"
NORDVPN_PASSWORD="ZjgyNWU2ZTgx"
OVPNPATH="."
OVPNPATH="/tmp/vpncl"
#OVPNPATH="/tmp/openvpncl"
INTERFACE1='tun1'
INTERFACE2='vlan2'
TARGET='google.com'
SEARCH_FLAG=0
ACCUMULATOR1=0
ACCUMULATOR2=0
N_SAMPLES=2
MAX_RETRIES=20
i=1
j=1
RETRIES1=0
RETRIES2=0
input_args=${@}
arraylength=${#input_args[@]}
if [ $arraylength == "0" ] ; then
SEARCH_FLAG=1
fi
AL=2
AR=10
AU=13
AT=14
AZ=15
BE=21
BA=27
BR=30
BG=33
CA=38
CL=43
CR=52
HR=54
CY=56
CZ=57
DK=58
EG=64
EE=68
FI=73
FR=74
GE=80
DE=81
GR=84
HK=97
HU=98
IS=99
IN=100
ID=101
IR=104
IL=105
IT=106
JP=108
LV=119
LU=126
MK=128
MY=131
MX=140
MD=142
NL=153
NZ=156
NO=163
PL=174
PT=175
RO=179
RU=180
RS=192
SG=195
SK=196
SI=197
ZA=200
KP=114
ES=202
SE=208
CH=209
TW=211
TH=214
TR=220
UA=225
AE=226
UK=227
US=228
VN=234
function usage {
	echo
	echo "Usage : vpn-select.sh "
	echo
	echo "   e.g  vpn-select.sh"
	echo "                  with no options specified vpn-select.sh will measure
                  the latency difference through the VPN and change to 
                  the recommended server if above the value of the
                  environment variable \$MAX_DEVIATION_PERCENT default 250%"
	echo
	echo "   e.g  vpn-select.sh US "
	echo "                  with a valid 2 letter country code country code specified 
                  vpn-select.sh will immediately select the recommended 
                  server from the specified country"
	echo
	echo "   e.g  vpn-select.sh pt18 "
	echo "                  with a valid server name specified vpn-select.sh will 
                  immediately select the specified server"
	echo
	echo "	Environment Variables"
	echo " 		  \$MAX_DEVIATION_PERCENT set this variable to adjust the 
                  maximum permissible deviation between VPN and ISP latency"
	exit
}
eval country_code="\$$input_args"
eval country_name="\$$country_code"
TWOCHARS=`echo $input_args | grep -xE '^\w{2}'`
MANYCHARS=`echo $input_args | grep -xE '^\w{2}\w+'`
ARG_SERVER=0
ARG_COUNTRY=0

echo
if [ $TWOCHARS ]
then
	if [ $country_code ]
	then
		echo "\"$input_args($country_code)\" is a valid country code"
		ARG_COUNTRY=1
		SEARCH_FLAG=1
	else
		echo "\"$input_args\" is not a valid country code"
		usage
	fi
elif [ $MANYCHARS ]
then
	echo "$input_args has many chars"
	RECOMMENDED_SERVER="$input_args.nordvpn.com"
	echo $RECOMMENDED_SERVER
	ARG_SERVER=1
fi


if [ -n "$LANGUAGE" ]; then
	# LANGUAGE env variable is not availavle on buxybox ddwrt ash shell
	echo "---------------------------------------------------------------------------"
	echo "testing on localhost"	
	# test on localhost
	INTERFACE1='wlp2s0'
	INTERFACE2='wlp2s0'
	SEARCH_FLAG=1
fi
echo "---------------------------------------------------------------------------"
echo "Target        : $TARGET"
echo "1st interface : $INTERFACE1"
echo "2nd interface : $INTERFACE2"

if [ -n "$MAX_DEVIATION_PERCENT" ]; then
	echo "environment variable MAX_DEVIATION_PERCENT set to $MAX_DEVIATION_PERCENT%"
else
	MAX_DEVIATION_PERCENT=250
	echo "environment variable MAX_DEVIATION_PERCENT not set using default value $MAX_DEVIATION_PERCENT%"
fi

echo "---------------------------------------------------------------------------"

VPNRUNNING=`ping -I $INTERFACE1 $TARGET -c 1 -q | tail -n 1 | grep -Eo 'bad address.+' | tail -n 1`
if [ $VPNRUNNING ] ; then
	echo "openvpn not running properly ..."
	echo "restarting openvpn"
	killall openvpn
	sleep 3
fi
PROCESS_ID=`pidof openvpn`
if [ $PROCESS_ID ] ; then
	if [ ! $ARG_COUNTRY -a ! $ARG_SERVER ]
	then 
		while [ $i -le $N_SAMPLES ] 
		do

			RESULT1=`ping -I $INTERFACE1 $TARGET -c 1 -q | tail -n 1 | grep -Eo '[0-9]+.[0-9]+' | tail -n 1`
				if [ $RESULT1 ]; then
				ACCUMULATOR1=`awk "BEGIN { print $ACCUMULATOR1 + $RESULT1 }"`
				AVERAGE1=`awk "BEGIN { print $ACCUMULATOR1 / $i }"`
				echo "$i Latency of $INTERFACE1 is $RESULT1 : $ACCUMULATOR1 : $AVERAGE1 "
				i=`awk "BEGIN { print $i + 1 }"`	
			else
				echo "retry"
				RETRIES1=`awk "BEGIN { print $RETRIES1 + 1 }"`
				if [ $RETRIES2 -gt $MAX_RETRIES ] ; then
					break
				fi
				continue
			fi
			
		done
		echo "---------------------------------------------------------------------------"
		while [ $j -le $N_SAMPLES ]
		do
			RESULT2=`ping -I $INTERFACE2 $TARGET -c 1 -q | tail -n 1 | grep -Eo '[0-9]+.[0-9]+' | tail -n 1`
			if [ $RESULT2 ]; then
				ACCUMULATOR2=`awk "BEGIN { print $ACCUMULATOR2 + $RESULT2 }"`
				AVERAGE2=`awk "BEGIN { print $ACCUMULATOR2 / $j }"`
				echo "$j Latency of $INTERFACE2 is $RESULT2 : $ACCUMULATOR2 : $AVERAGE2 "
				j=`awk "BEGIN { print $j + 1 }"`	
			else
				echo "retry"
				RETRIES2=`awk "BEGIN { print $RETRIES2 + 1 }"`
				if [ $RETRIES2 -gt $MAX_RETRIES ] ; then
					break
				fi
				continue
			fi
			
		done
		echo "---------------------------------------------------------------------------"
		echo "Average Latency : $INTERFACE1 $TARGET $AVERAGE1 `awk "BEGIN { print $i-1 }"` samples, $RETRIES1 retries"
		echo "                  $INTERFACE2 $TARGET $AVERAGE2 `awk "BEGIN { print $j-1 }"` samples, $RETRIES2 retries"
		if [ $RETRIES1 -gt $RETRIES2 ] ; then
			DR=`awk "BEGIN { print $RETRIES1 - $RETRIES2 }"` 
			DRPRC=`awk "BEGIN { print $DR  / ( $RETRIES2 / 100 ) }"` 
			echo "Deviation       : $INTERFACE1 has $DRPRC % ( $DR ) more retries than $INTERFACE2 "
		elif  [ $RETRIES2 -gt $RETRIES1 ] ; then
			DR=`awk "BEGIN { print $RETRIES2 - $RETRIES1 }"` 
			DRPRC=`awk "BEGIN { print $DR  / ( $RETRIES1 / 100 ) }"`
			echo "Deviation       : $INTERFACE2 has $DRPRC % ( $DR ) more retries than $INTERFACE1 "
		else 
			echo "Deviation"
		fi

		AVERAGE1=`echo $AVERAGE1 | awk '{printf "%.f\n", int($1+0.5)}'`
		AVERAGE2=`echo $AVERAGE2 | awk '{printf "%.f\n", int($1+0.5)}'`

		if [ $AVERAGE1 -gt $AVERAGE2 ] ; then
			DELTA=`awk "BEGIN { print $AVERAGE1 - $AVERAGE2 }"` 
			DELTAPRC=`awk "BEGIN { print $DELTA  / ( $AVERAGE2 / 100 ) }"` 
			DELTAPRCROUND=`echo $DELTAPRC | awk '{printf "%d\n", int($1+0.5)}'`
			echo "                : $INTERFACE1 is $DELTAPRC % ( $DELTA ) slower than $INTERFACE2 "
		else
			DELTA=`awk "BEGIN { print $AVERAGE2 - $AVERAGE1 }"` 
			DELTAPRC=`awk "BEGIN { print $DELTA  / ( $AVERAGE1 / 100 ) }"`
			DELTAPRCROUND=`echo $DELTAPRC | awk '{printf "%d\n", int($1+0.5)}'`
			echo "                : $INTERFACE2 is $DELTAPRC % ( $DELTA ) slower than $INTERFACE1 "
		fi
		echo "---------------------------------------------------------------------------"

		if [ $DELTAPRCROUND -gt $MAX_DEVIATION_PERCENT ] ; then
			echo "maximum deviation of $MAX_DEVIATION_PERCENT% exceeded"
			SEARCH_FLAG=1
		fi
	fi
else
	echo "---------------------------------------------------------------------------"
	echo "vpn off "
	SEARCH_FLAG=0
fi


echo "SF $SEARCH_FLAG"
if [ $SEARCH_FLAG == "1" ] ; then
	if [ $ARG_COUNTRY == "1" ] ;
	then
		# https://blog.sleeplessbeastie.eu/2019/01/14/how-to-use-terminal-to-display-servers-recommended-by-nordvpn/
		echo $country_code
		echo "searching for $input_args($country_code) server recommendation via interface $INTERFACE2"
		echo "---------------------------------------------------------------------------"
		RECOMMENDED_SERVER=`curl --interface $INTERFACE2 --silent -k "https://nordvpn.com/wp-admin/admin-ajax.php?action=servers_recommendations&filters=\{%22country_id%22:$country_code\}" | grep -Eo '\"hostname"\:\"\w+.nordvpn.com\"' | grep -Eo '\w+.nordvpn.com' | head -n 1`
	fi
	
	if [ $ARG_SERVER == "0" -a  $ARG_COUNTRY == "0" ] ;
	then
		echo "searching for new server recommendation via interface $INTERFACE2"
		echo "---------------------------------------------------------------------------"
		RECOMMENDED_SERVER=`curl --interface $INTERFACE2 --silent -k 'https://nordvpn.com/wp-admin/admin-ajax.php?action=servers_recommendations'  | grep -Eo '\"hostname"\:\"\w+.nordvpn.com\"' | grep -Eo '\w+.nordvpn.com' | head -n 1`
	fi

	echo "recommended server  : $RECOMMENDED_SERVER"
	CF_PREPEND="https://downloads.nordcdn.com/configs/files/ovpn_udp/servers/"
	CF_APPEND=".udp.ovpn"
	CONFIG_FILE_URL=$CF_PREPEND$RECOMMENDED_SERVER$CF_APPEND
	CF_URL_CHECK=`curl -s -o /dev/null -w "%{http_code}" $CONFIG_FILE_URL`
	echo "CF_URL_CHECK : $CF_URL_CHECK"
	if [ "$CF_URL_CHECK" == "200" -o  "$CF_URL_CHECK" == "000" ] ;
	then
		echo "$CONFIG_FILE_URL exists"
	else
		echo "$CONFIG_FILE_URL does not exist"
		echo
		echo "$input_args is not a valid server name"
		usage
	fi
	 
	echo "download link       : $CONFIG_FILE_URL"
	echo "---------------------------------------------------------------------------"
	FILENAME=$RECOMMENDED_SERVER$CF_APPEND
	if [ -f "$FILENAME" ]
	then
		echo "file $FILENAME found."
		FILE_LOCAL_CTIME=`date -r $FILENAME +%s`
		HEADER=`curl --silent -k --interface $INTERFACE2 --head $CONFIG_FILE_URL`
		MODIFIED=`echo "$HEADER" | grep -Eoi '^last-modified.+' | tr 'A-Z' 'a-z' | sed 's/^last-modified: //'`
		REMOTE_CTIME=`date -d "$MODIFIED" -D "%a, %d %b %Y %T" +'%Y-%m-%d %H:%M:%S'`
		FILE_REMOTE_CTIME=`date --date="$REMOTE_CTIME" +%s`
		LOCAL_CTIME=`date -d @$FILE_LOCAL_CTIME`
		if [ $FILE_REMOTE_CTIME -gt $FILE_LOCAL_CTIME ] ; then
			echo "remote file $FILENAME newer"
			echo " remote $MODIFIED"
			echo " local  $LOCAL_CTIME"
			echo "downloading new file : $CONFIG_FILE_URL"
			echo "---------------------------------------------------------------------------"
			curl -O -k $CONFIG_FILE_URL
		else 
			echo "local file $FILENAME up to date"
			echo " remote   : $FILE_REMOTE_CTIME / $MODIFIED "
			echo " local    : $FILE_LOCAL_CTIME / $LOCAL_CTIME"
		fi

	else
		echo "file $FILENAME does not exist."
		echo "downloading new file : $CONFIG_FILE_URL"
		echo "---------------------------------------------------------------------------"
		curl -O -k $CONFIG_FILE_URL
	fi
else
	usage
fi 
echo "---------------------------------------------------------------------------"
echo "procecssing configuration for $FILENAME"
echo

#### Ensure gui client disabled ####
if [ `nvram get openvpncl_enable` != 0 ]; then
	echo "Disable gui client"
	nvram set openvpncl_enable=0
	nvram commit
	sleep 10
	echo "Disabled gui client"
fi

if [ ! -d $OVPNPATH ]; then
  mkdir -p $OVPNPATH;
fi

OVPNCONFIG="ca /tmp/openvpncl/ca.crt
management 127.0.0.1 16
management-log-cache 100
verb 3
mute 3
syslog
writepid /var/run/openvpncl.pid
client
resolv-retry infinite
nobind
persist-key
persist-tun
script-security 2
dev tun1
proto udp4
cipher aes-256-cbc
auth sha512
auth-user-pass /tmp/openvpncl/credentials
remote Fo.LE.NA3E 1194
comp-lzo yes
tun-mtu 1500
mtu-disc yes
fast-io
tls-auth /tmp/openvpncl/ta.key 1
remote-cert-tls server
remote-random
nobind
tun-mtu 1500
tun-mtu-extra 32
mssfix 1450
persist-key
persist-tun
ping-timer-rem
reneg-sec 0

log /tmp/vpn.log

#Delete `#` in the line below if your router does not have credentials fields and you followed the 3.1 step:
#auth-user-pass /tmp/openvpncl/user.conf"

OVPNROUTEUP="#!/bin/sh
iptables -D POSTROUTING -t nat -o tun1 -j MASQUERADE
iptables -I POSTROUTING -t nat -o tun1 -j MASQUERADE
iptables -D INPUT -i tun1 -j ACCEPT
iptables -D FORWARD -i tun1 -j ACCEPT
iptables -D FORWARD -o tun1 -j ACCEPT
iptables -I INPUT -i tun1 -j ACCEPT
iptables -I FORWARD -i tun1 -j ACCEPT
iptables -I FORWARD -o tun1 -j ACCEPT
"

OVPNROUTEDOWN="#!/bin/sh
#!/bin/sh
iptables -D INPUT -i tun1 -j ACCEPT
iptables -D POSTROUTING -t nat -o tun1 -j MASQUERADE
"
OVPNCRED="$NORDVPN_USERNAME
$NORDVPN_PASSWORD"
if [ ! -f $OVPNPATH/openvpn.conf ]; then
	echo  $OVPNCONFIG > $OVPNPATH/openvpn.conf
fi

if [ ! -f $OVPNPATH/route-up.sh ]; then
	echo $OVPNROUTEUP > $OVPNPATH/route-up.sh
fi

if [ ! -f $OVPNPATH/route-down.sh ]; then
	echo $OVPNROUTEDOWN > $OVPNPATH/route-down.sh
fi

if [ ! -f $OVPNPATH/credentials ]; then
	echo $OVPNCRED > $OVPNPATH/credentials
fi

nvram set openvpncl_user=$NORDVPN_USERNAME
nvram commit
nvram set openvpncl_pass=$NORDVPN_PASSWORD
nvram commit
nvram set openvpncl_remoteip=$RECOMMENDED_SERVER
nvram commit

cp $OVPNPATH/ta.key $OVPNPATH/ta.key.bak
cp $OVPNPATH/ca.crt $OVPNPATH/ca.crt.bak
TLSAUTH=`sed -n '/^-----BEGIN OpenVPN Static key V1-----$/,/^-----END OpenVPN Static key V1-----$/p' $FILENAME`
echo "$TLSAUTH"
echo "$TLSAUTH" > $OVPNPATH/ta.key
nvram set openvpncl_tlsauth="${TLSAUTH}"
nvram commit
CERTIFICATE=`sed -n '/^-----BEGIN CERTIFICATE-----$/,/^-----END CERTIFICATE-----$/p' $FILENAME`
echo "$CERTIFICATE" > $OVPNPATH/ca.crt
nvram set openvpncl_ca="${CERTIFICATE}"
nvram commit


sed -i.bak -re "s/(remote \w+\.\w+\.\w+)/remote $RECOMMENDED_SERVER/g" $OVPNPATH/openvpn.conf
sed -i.bak -re "s~ca .*ca.crt~ca $OVPNPATH\/ca.crt~g" $OVPNPATH/openvpn.conf
sed -i.bak -re "s~auth-user-pass .*credentials~auth-user-pass $OVPNPATH\/credentials~g" $OVPNPATH/openvpn.conf
sed -i.bak -re "s~tls-auth .*ta.key 1~tls-auth $OVPNPATH\/ta.key 1~g" $OVPNPATH/openvpn.conf
sed -i.bak -re "s~auth-user-pass .*user.conf~auth-user-pass $OVPNPATH\/user.conf~g" $OVPNPATH/openvpn.conf
echo "---------------------------------------------------------------------------"
echo "writing certificate to $OVPNPATH/ca.crt "
echo
cat $OVPNPATH/ca.crt
echo
echo "writing key to $OVPNPATH/ta.key "
echo
cat $OVPNPATH/ta.key
echo
echo "changing remote server to $RECOMMENDED_SERVER in $OVPNPATH/openvpn.conf "
echo
cat $OVPNPATH/openvpn.conf
echo "---------------------------------------------------------------------------"
chmod 600 $OVPNPATH/ca.crt $OVPNPATH/ta.key $OVPNPATH/credentials $OVPNPATH/openvpn.conf; chmod 700 $OVPNPATH/route-up.sh $OVPNPATH/route-down.sh
curl ipinfo.io
echo
echo "kill openvpn processes"
killall openvpn
sleep 3
echo "start openvpn daemon"
openvpn --verb 3 --config $OVPNPATH/openvpn.conf --route-up $OVPNPATH/route-up.sh --down-pre --down $OVPNPATH/route-down.sh --daemon
PROCESS=`ps | grep openvpn | head -n 1`
echo "started : $PROCESS"
sleep 3
echo "VPNRUN $INTERFACE1 $TARGET"
#### Enable gui client  ####
if [ `nvram get openvpncl_enable` != 1 ]; then
	echo "Enable gui client"
	nvram set openvpncl_enable=1
	nvram commit
	sleep 10
	echo "Enabled gui client"
fi
sleep 3
VPNRUNNING=`ping -I $INTERFACE1 $TARGET -c 1 -q | tail -n 1 | grep -Eo 'bad address.+' | tail -n 1`
if [ $VPNRUNNING ] ; then
	echo "openvpn failed to start "
else
	echo "openvpn running on $RECOMMENDED_SERVER server ..."
fi


curl ipinfo.io
echo
exit 0

